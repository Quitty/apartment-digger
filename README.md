# Apartment digger

Is a barebones implementation of ImmobilienScout24's API and Google's maps and geocoding, 
meant to circumvent some of the stupid inherent to Immobilien's filtering.

Every possible corner has been cut. You've been warned.

## Quickstart

* Install mongoDB
* `./build.sh`
* edit `./config/default.json` (mainly the search perferences, 
although i'd appreciate it if you used your own API key) 
* `./run.sh`
* browse to `localhost:4242`
* click 'Reload apartments'
* watch the cli logs
* refresh (hard-refresh only or the lazy-loading will fuck-up the CSS)

## Notes

* Ignore the errors. Yes, really.
* Yellow markers are for short term. White for long. Gray for irrelevant.
* Existing apartments will not be re-imported. 
That means you won't abuse the API quotas, but it also means existing apartment info is not updated.
That's not usually a problem, but FYI
* The import process auto-filters apartments who explicitly refuse pets, or are on the ground floor.
See `immobilien.js:isValid` if you want to change that.
* Changing the center-point is a PITA on Immobilien's side, which is why Kreuzberg is hard-coded.
It's fixable, but it's probably easier to increase the radius.
* Changes to the `relevant` checkbox, for example, are persisted when you save - 
i used it to 'mark off' apartments, because deleting them will cause them to be re-imported
* The filter is free-text and runs over the entire object - meaning it has false positives
* Filtering on boolean values like that is tricky, so 'Clear selection' 
and put 'yellow' in filter to filter on yellow icons (for short term), or 'white' (for long term)
* Many apartments do not have accurate addresses and are therefore missing from Immobilien's map page,
because duh. This map puts them in the center of their zip code for lack of better options.
When several markers occupy the same spot, they are offset by a bit. You may have to zoom in.