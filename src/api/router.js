const express = require('express');
const router = new express.Router({});

const apartments = require('../data/entities/apartment');
const dataSource = require('../core/services/api/immobilien');

router.use('/alive', function(request, response, next) {
    if (request.method === 'GET') {
        response.send('Alive');
    }
    next();
});

router.use('/apartments', function(request, response, next) {
    let result;
    switch (request.method) {
        case 'POST':
            result = apartments.new(request.body);
            break;
        case 'DELETE':
            result = apartments.delete(request.body);
            break;
        case 'GET':
            result = apartments.get(request.query);
            break;
        case 'PATCH':
            result = apartments.update(request.body);
            break;
        case 'PURGE':
            result = dataSource.immobilien.loadApartments(request.query.isLongTerm === 'true');
            break;
        default:
            next();
    }

    result.then(function(result) {
        response.contentType('application/javascript').send(result);
    }, function(error) {
        response.send(error, 500);
    });

});

module.exports = router;