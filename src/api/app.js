const logger = require('../core/helpers/logger');
const morgan = require('morgan');

const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const cors = require('cors');
const path = require('path');

const router = require('./router');
const port = config.get('api.port');

const app = express();

app.use(morgan('combined', { stream: logger.stream}));
app.use(bodyParser.json());
app.use(cors({
    origin: config.get('api.corsOrigin'),
    optionsSuccessStatus: 200
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static('public'));

app.use(router);

app.listen(port, function() {
    console.log('Listening on port ' + port);
});
