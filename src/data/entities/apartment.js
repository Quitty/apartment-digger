const storageClient = require('../storage');

// noinspection JSUnresolvedVariable
const Apartment = storageClient.getModel(
    name = 'apartment',
    schema={
        _id: Number,
        isActive: Boolean,
        link: String,
        address: String,
        size: Number,
        rooms: Number,
        pictures: Array,
        floor: Number,
        topFloor: Number,
        price: Number,
        company: String,
        comments: String,
        startDate: String,
        pets: String,
        latitude: Number,
        longitude: Number,
        isApproximateAddress: Boolean,
        isLongTerm: Boolean,
    },
    collection='apartments'
);

module.exports.new = function (record) {
    return storageClient.put(Apartment, record);
};
module.exports.delete = function (record) {
    return storageClient.delete(Apartment, record);
};
module.exports.get = function (query) {
    return storageClient.get(Apartment, query);
};
module.exports.update = function (record) {
    return storageClient.get(Apartment, {_id: record._id}).then(function (item) {
        item = item[0].toObject();
        Object.assign(item, record);

        return storageClient.update(Apartment, item);
    }, function (error) {
        return new Promise(error);
    });
};
