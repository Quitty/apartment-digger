const config = require('config');
const logger = require('../../core/helpers/logger');

function processResponse(error, item) {
    if (error) {
        logger.error('Error performing Mongo transaction.', {error: error});
        return "Error";
    } else {
        return item;
    }
}

function toObject(type, record) {
    return new type(record);
}

function Mongo() {
    const storageClient = require('mongoose');
    storageClient.Promise = require('bluebird');

    storageClient.connect(config.get('storage.mongodb.connection'), {useMongoClient: true}).then(
        () => logger.debug('Mongo started up'),
        error => {
            logger.error('Mongo failed to start: ' + error);
            process.exit(1);
        }
    );

    return {
        _toObject: toObject,
        registerSchema: function (model, schema, collection) {
            return storageClient.model(model, schema, collection);
        },
        put: function (type, record) {
            return toObject(type, record).save(processResponse);
        },
        get: function (type, query) {
            return type.find(query);
        },
        delete: function (type, record) {
            return type.remove(record, processResponse);
        },
        update: function (type, record) {
            let recordId = record._id;
            delete recordId._id;

            return type.update({_id: recordId}, {$set: record});
        }
    };
}

module.exports = Mongo();
