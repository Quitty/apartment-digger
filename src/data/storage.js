const client = require('./storage/mongo');

// Interface
module.exports._toObject = client._toObject;
module.exports.getModel = client.registerSchema;
module.exports.get = client.get;
module.exports.put = client.put;
module.exports.delete = client.delete;
module.exports.update = client.update;
