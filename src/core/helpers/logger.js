const winston = require('winston');
const config = require('config');

const outputs = {
    'console': new winston.transports.Console({format: winston.format.simple()})
};
const logger = winston.createLogger({
    level: config.get('log.level') || 'warn',
    handleExceptions: true,
    colorize: true,
    transports: [
        outputs[config.get('log.output')]
    ]
});

logger.stream = {
    write: function(message, encoding) {
        logger.debug(message);
    }
};

module.exports = logger;