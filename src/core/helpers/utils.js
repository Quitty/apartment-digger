const _ = require('lodash');
const util = require('util');

// Object.prototype.renameKeys = function (conversionMap) {
//     return _.fromPairs(_.toPairs(this).map(
//         function (tuple) {
//             return [conversionMap[tuple[0]] || tuple[0], tuple[1]]
//         }
//     ));
// };

String.prototype.normalizeNumeric = function () {
    return this.split(' ')[0].replace('.', '').replace(',','.');
};

String.prototype.format = function(...args) {
    return util.format(this.toString(), ...args);
};
