const _ = require('lodash');

let basePayload = {
    minRadius: null,
    travelTime: 'NONE',
    shapes: null,
    furnishing: null,
    flatShareSize: null,
    shortTermAccommodationType: null,
    locationSelectionType: 'VICINITY',
    numberOfPersons: null,
    vendorGroup: null,
    minimumInternetSpeed: null,
    beginRent: null,
    rentalPeriod: null,
    lastModifiedAfter: null,
    rentDurationInMonths: null,
    toplisted: null,
    wohnberechtigungsscheinNeeded: null,
    hasRented: null,
    trailLivingPossible: null,
    withFurniture: null,
    smokingPermitted: null,
    handoverPermitted: null,
    firstActivationRange: null,
    latestBeginRentRange: null,
    priceRangeWithType: null,
    virtualTourType: null,
    yearOfConstructionRange: null,
    numberOfParkingSpacesRange: null,
    numberOfSeatsRange: null,
    numberOfBedsRange: null,
    floorRange: null,
    budgetRentRange: null,
    priceMultiplierRange: null,
    netAreaRange: null,
    netRentRange: null,
    lotSizeRange: null,
    totalAreaRange: null,
    siteAreaRange: null,
    pricePerSqm: null,
    priceRange: null,
    marketValueRange: null,
    onlyShortTermBuildable: false,
    onlyBuildingProject: false,
    onlyNewBuildingOrBuildingProject: false,
    onlyWithBalcony: false,
    onlyWithInternet: false,
    onlyWithGarden: false,
    onlyWithElevator: false,
    onlyWithParking: false,
    onlyWithBarrierFree: false,
    onlyWithGuestToilet: false,
    onlyWithBasement: false,
    onlyWithLodgerFlat: false,
    onlyWithAvailableHighVoltageCurrent: false,
    onlyWithAmbulantNursingService: false,
    onlyWithPictures: false,
    onlyYellowPageEntries: false,
    onlyHandicappedAccessible: false,
    onlySecondAuctions: false,
    onlySplittingAuctions: false,
    onlyWithCareOfAlzheimerDiseasePatients: false,
    onlyWithCareOfArtificalRespirationPatients: false,
    onlyWithCareOfDementiaPatients: false,
    onlyWithCareOfMultipleSclerosisPatients: false,
    onlyWithCareOfParkinsonsDiseasePatients: false,
    onlyWithCareOfStrokePatients: false,
    onlyWithCareOfVegetativeStatePatients: false,
    onlyWithCooker: false,
    onlyWithCookingPossibility: false,
    onlyWithCraneRails: false,
    onlyWithDishWasher: false,
    onlyWithFridge: false,
    onlyWithHoist: false,
    onlyWithLiftingPlatform: false,
    onlyWithOven: false,
    onlyWithOwnFurnishingPossible: false,
    onlyWithRamp: false,
    onlyWithWashingMachine: false,
    onlyWithoutCourtage: false,
    onlyWithAirConditioning: false,
    onlyWithItInfrastructure: false,
    onlyWithKitchen: false,
    onlyWithPlanningPermission: false,
    onlyFlatShareSuitable: false,
    onlyWithShowcaseOrPremium: false,
    realEstateIds: [],
    neighbourhoodIds: [],
    apartmentTypes: [],
    houseTypes: [],
    houseTypeTypes: [],
    officeTypes: [],
    tradeSiteUtilizations: [],
    gastronomyTypes: [],
    storeTypes: [],
    specialPurposePropertyTypes: [],
    investObjectTypes: [],
    industryTypes: [],
    garageTypes: [],
    careTypes: [],
    auctionObjectTypes: [],
    siteDevelopmentTypes: [],
    siteConstructibleTypes: [],
    constructionPhaseTypes: [],
    seniorCareLevels: [],
    roomTypes: [],
    heatingTypes: [],
    locationClassifications: [],
    officeRentDurations: [],
    energyEfficiencyClasses: [],
    petsAllowedTypes: [],
    buildingProjectId: null,
    vicinitySearch: true,
    travelTimeSearch: false,
    handicappedAccessible: false,
    shapeSearch: false,
    geoHierarchySearch: false,
    historicalSearch: false,
    premiumDeveloperProject: false,
    view: 'IS24'
};

function setCustomData(radius, centerPoint, minRooms) {
    return _.extend(basePayload, {
        radius: radius,
        centerX: centerPoint.x,
        centerY: centerPoint.y,
        centerOfSearchAddress: {
            region: null,
            city: centerPoint.name,
            quarter: null,
            postcode: null,
            street: null
        },
        numberOfRoomsRange: {
            min: minRooms
        },
    });
}

module.exports.shortTermPayload = function(radius, centerPoint, maxRent, minRooms) {
    return _.extend(setCustomData(radius, centerPoint, minRooms), {
        realEstateType: 'SHORT_TERM_ACCOMMODATION',
        totalRentRange: {
            max: maxRent
        }
    });
};

module.exports.longTermPayload = function(radius, centerPoint, maxRent, minRooms, minArea) {
    return _.extend(setCustomData(radius, centerPoint), {
        realEstateType: "APARTMENT_RENT",
        netAreaRange: {
            min: minArea
        },
        netRentRange: {
            max: maxRent - 200
        }
    });
};