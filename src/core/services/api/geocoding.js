const config = require('config');
const request = require('request-promise');
const _ = require('lodash');
// const logger = require('../../../core/helpers/logger');

class Geocoder {
    constructor() {
        this.API = {
            base: 'https://maps.googleapis.com/maps/api/geocode',
            key: config.get('keys.googleAPIKey'),
            actions: {
                json: '/json'
            }
        };
    }

    getApproximateCoordinates(address) {
        return request({
            method: 'GET',
            json:true,
            uri: '%s%s?address=%s&key=%s'.format(
                this.API.base,
                this.API.actions.json,
                _.replace(address, ' ', '+'),
                this.API.key
            )
        }).then(response => {
            return response.results[0].geometry.location;
        });
    }
}

const geocoder = new Geocoder();
module.exports = geocoder;
