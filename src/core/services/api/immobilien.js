const request = require('request-promise');
const cheerio = require('cheerio');
const _ = require('lodash');
const storage = require('../../../data/entities/apartment');
const immobilienSearch = require('../models/immobilienSearch');
const logger = require('../../../core/helpers/logger');
const geocoder = require('./geocoding');
const config = require('config');

require('../../../core/helpers/utils');

class Immobilien {
    constructor() {
        this.searchParameters = {
            centerPoint: {
                name: 'Berlin-Kreuzberg (Kreuzberg)',
                x: 227200,
                y: 2509904
            }
        };
        Object.assign(this.searchParameters, config.get('searchPreferences'));

        this.API = {
            base: 'https://www.immobilienscout24.de',
            actions: {
                getSearchURL: '/Suche/controller/search/change.go?sortingCode=0&otpEnabled=true',
                searchApartments: '/Suche/controller/asyncResults.go',
                getInternalData: '/expose'
            }
        };

        this.storedApartments = [];

        this.getSearchURL();
    }

    getSearchURL() {
        logger.debug('Retrieving search URL', {
            parameters: this.searchParameters
        });

        request({
            method: 'POST',
            uri: '%s%s'.format(this.API.base, this.API.actions.getSearchURL),
            //TODO: convert to ...
            body: immobilienSearch.shortTermPayload(
                this.searchParameters.searchRadius,
                this.searchParameters.centerPoint,
                this.searchParameters.maxPrice,
                this.searchParameters.minRooms
            ),
            json: true
        }).then(response => {
            logger.info('Ready for short-term search');
            this.shortTermSearchURL = response.url;
        });

        request({
            method: 'POST',
            uri: '%s%s'.format(this.API.base, this.API.actions.getSearchURL),
            //TODO: convert to ...
            body: immobilienSearch.longTermPayload(
                this.searchParameters.searchRadius,
                this.searchParameters.centerPoint,
                this.searchParameters.maxPrice,
                this.searchParameters.minRooms,
                this.searchParameters.minArea
            ),
            json: true
        }).then(response => {
            logger.info('Ready for long-term search');
            this.longTermSearchURL = response.url;
        });
    }

    getApartments(isLongTerm, page) {
        let searchPageURL = isLongTerm
            ? this.longTermSearchURL
            : this.shortTermSearchURL;
        if (!searchPageURL) throw Error('Search URL not yet resolved');

        searchPageURL = '%s%s'.format(
            this.API.base,
            _.replace(searchPageURL, 'S-T/', 'S-T/P-%d/'.format(page || 1))
        );

        logger.info('Retrieving results for page %d'.format(page || 1));
        logger.debug('Page address: %s'.format(searchPageURL));

        return request({
            method: 'POST',
            uri: searchPageURL,
            json: true
        }).then(response => {
            let numberOfPages = response.searchResponseModel['resultlist.resultlist'].paging.numberOfPages;
            let results = response.searchResponseModel['resultlist.resultlist'].resultlistEntries[0].resultlistEntry;
            if ('@id' in results) {
                results = [results];
            }
            results.filter(apartment =>
                !this.storedApartments.includes(apartment['@id'])
            ).forEach(apartment =>
                this.storedApartments.push(
                    this.processApartment(apartment, isLongTerm))
            );

            if (!page) {
                logger.info(
                    'Found a total of %d result pages'.format(numberOfPages));
                _.range(2, numberOfPages + 1).forEach(pageNumber =>
                    this.getApartments(isLongTerm, pageNumber)
                );
            }
        }, (error) => {
            logger.error('result page %d retrieval error.'.format(page),
                {error: error});
        });
    }

    processApartment(apartmentRecord, isLongTerm) {
        logger.debug('Processing apartment %d'.format(apartmentRecord['@id']));

        let normalizedApartmentRecord = {
            _id: apartmentRecord['@id'],
            isActive: true,
            isLongTerm: isLongTerm
        };
        apartmentRecord = apartmentRecord['resultlist.realEstate'];

        _.extend(normalizedApartmentRecord, {
            size: apartmentRecord.livingSpace,
            rooms: apartmentRecord.numberOfRooms,
            price: apartmentRecord.price.value,
            company: apartmentRecord.contactDetails.company,
            startDate: apartmentRecord.startRentalDate,
            pets: null,
            floor: null,
            topFloor: null
        });

        if ('wgs84Coordinate' in apartmentRecord.address) {
            _.extend(normalizedApartmentRecord,
                apartmentRecord.address.wgs84Coordinate);
        } else {
            normalizedApartmentRecord.isApproximateAddress = true;
        }

        if ('galleryAttachments' in apartmentRecord) {
            let attachments = apartmentRecord.galleryAttachments.attachment;
            normalizedApartmentRecord.pictures = attachments[['@xlink.href']] ||
                attachments.map(picture => picture['@xlink.href']);
        }

        if (normalizedApartmentRecord.size >= this.searchParameters.minArea) {
            this.getPageData(normalizedApartmentRecord._id).then(data => {
                _.extend(normalizedApartmentRecord, data);
                if (this.isValid(normalizedApartmentRecord)) {
                    if (!normalizedApartmentRecord.isApproximateAddress) {
                        this.storeApartment(normalizedApartmentRecord);
                    } else {
                        let address = '%s %s'.format(
                            apartmentRecord.address.postcode,
                            apartmentRecord.address.city
                        );
                        geocoder.getApproximateCoordinates(address).
                            then(coordinates => {
                                _.extend(normalizedApartmentRecord, {
                                    latitude: coordinates.lat,
                                    longitude: coordinates.lng
                                });
                                this.storeApartment(normalizedApartmentRecord);
                            });
                    }
                }
            });
        }
    }

    storeApartment(normalizedApartmentRecord) {
        storage.new(normalizedApartmentRecord).then(() => {
            logger.info('stored apartment %d'.format(
                normalizedApartmentRecord._id
            ));
            this.storedApartments.push(normalizedApartmentRecord._id);
        });
    }

    isValid(apartment) {
        let isPetsAllowed = (pets) => pets !== ' Nein ';
        return isPetsAllowed(apartment.pets) &&
            apartment.size >= this.searchParameters.minArea &&
            apartment.floor !== 0;
    }

    getPageData(apartmentID) {
        return request({
            method: 'GET',
            uri: '%s%s/%d'.format(
                this.API.base,
                this.API.actions.getInternalData,
                apartmentID
            )
        }).then(result => {
            let $ = cheerio.load(result);
            let floor = $('dd[class^=is24qa-etage]').text().split(' von ');
            return {
                pets: $('dd[class^=is24qa-haustiere]').text(),
                floor: parseInt(floor[0] || -1),
                topFloor: parseInt(floor[1] || -1)
            };
        }, (error) => {
            logger.error('page data retrieval error.', {error: error});
        });
    }

    getExistingApartments() {
        return storage.get().then(apartments => {
            apartments.map(apartment => apartment.id).forEach(
                apartmentId => this.storedApartments.push(apartmentId)
            );
            return apartments;
        });
    }

    loadApartments(isLongTerm) {
        return this.getExistingApartments().then(
            this.getApartments(isLongTerm).then(
                this.getExistingApartments()
            )
        );
    }
}

module.exports.immobilien = new Immobilien();