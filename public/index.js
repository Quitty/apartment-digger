'use strict';

angular.module('apartmentDigger', ['ngMap', 'ui.bootstrap'])
    .controller('mainController', function ($scope, $http, $modal, NgMap) {
        let ctl = this;
        let apiUrl = 'http://localhost:4242/apartments';
        let apartmentIcons = {
            white: 'http://labs.google.com/ridefinder/images/mm_20_white.png',
            yellow: 'http://labs.google.com/ridefinder/images/mm_20_yellow.png',
            grey: 'http://labs.google.com/ridefinder/images/mm_20_gray.png',
            red: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
        };

        ctl.searchText = "";
        ctl.apartmentTerm = 'short';

        function getApartmentIcon(apartment) {
            return apartment.isActive ?
                (apartment.isLongTerm ? apartmentIcons.white : apartmentIcons.yellow)
                : apartmentIcons.grey;
        }

        function updateMarkers() {
            $http({
                method: 'GET',
                url: apiUrl
            }).then(function (apartments) {
                let points = [];
                apartments.data.map(function (apartment) {
                    apartment.coordinates = [apartment.latitude, apartment.longitude];
                    apartment.icon = getApartmentIcon(apartment);
                    apartment.originalIcon = apartment.icon;
                    if (points.includes(apartment.coordinates.join(','))) {
                        apartment.coordinates[1] += 0.0001;
                    }
                    points.push(apartment.coordinates.join(','));

                    return apartment;
                });
                ctl.positions = apartments.data;


                NgMap.getMap().then(function (map) {
                    map.setCenter(ctl.getCurrentMarker());
                });
            });
        }

        updateMarkers();

        ctl.setCurrentMarker = function (mapItem, point) {
            if ('currentMarker' in ctl) {
                ctl.currentMarker.icon = ctl.currentMarker.originalIcon;
            }
            point.icon = apartmentIcons.red;
            ctl.currentMarker = point;
        };

        ctl.getCurrentMarker = function () {
            if (ctl.positions) {
                return ["52.523323723775135", "13.39057445526123"];
            }
        };

        ctl.updateCurrentMarker = function (currentMarker) {
            $http({
                method: 'PATCH',
                url: apiUrl,
                headers: {
                    'content-type': 'application/json'
                },
                data: currentMarker
            }).then(function (result) {
                console.log(result);
            });

            Object.assign(ctl.positions.filter(marker => marker._id === currentMarker._id)[0], {
                originalIcon: getApartmentIcon(currentMarker),
                isActive: currentMarker.isActive
            })
        };

        ctl.deleteCurrentMarker = function (currentMarker) {
            $http({
                method: 'DELETE',
                url: apiUrl,
                headers: {
                    'content-type': 'application/json'
                },
                data: currentMarker
            }).then(function (result) {
                console.log(result);
                updateMarkers();
            });
        };

        ctl.clearCurrentMarker = function () {
            if ('currentMarker' in ctl) {
                ctl.currentMarker.icon = ctl.currentMarker.originalIcon;
            }
            delete ctl.currentMarker;
        };

        ctl.reloadApartments = function () {
            $http({
                method: 'PURGE',
                url: apiUrl + '?isLongTerm=' + (ctl.apartmentTerm === 'long').toString()
            }).then(function (result) {
                console.log(result);
                updateMarkers();
            });
        };

    })
    .controller('newActivityController', function ($scope) {
        $scope.ctl.newMarker = {
            coordinates: [$scope.mapItem.latLng.lat().toString(), $scope.mapItem.latLng.lng().toString()]
        };
    });
